package com.riis.biogen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class BioActivity : AppCompatActivity() {

    private lateinit var bioTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bio)

        bioTextView = findViewById(R.id.textBio)

        val firstName = intent.getStringExtra("first_name")
        val lastName = intent.getStringExtra("last_name")
        val school = intent.getStringExtra("school")
        val yearOfGraduation = intent.getStringExtra("year_of_graduation")
        val selectedDegree = intent.getStringExtra("selected_degree")
        val selectedMajor = intent.getStringExtra("selected_major")
        val favouriteActivities = intent.getStringExtra("favourite_activities")

        bioTextView.text = "$firstName $lastName graduated in $yearOfGraduation with a $selectedDegree in $selectedMajor from $school. Their favourite activities are $favouriteActivities."



    }



}