package com.riis.biogen //package unique identifies the app on the device

//importing important libraries
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    //The MainActivity class inherits from the AppCompatActivity class which ensures
    //...that apps developed using newer Android versions are compatible with older ones
    //The MainActivity class also inherits from the AdapterView class

    //DEFINING CLASS VARIABLES
    //private var radioGroupsList: ArrayList<RadioGroup> = ArrayList()
    lateinit var radioButton: Button
    lateinit var createButton: Button
    lateinit var spinner: Spinner
    lateinit var radioGroup1: RadioGroup
    lateinit var radioGroup2: RadioGroup
    lateinit var radioGroup3: RadioGroup


    lateinit var firstName: String
    lateinit var lastName: String
    lateinit var school: String
    lateinit var gradYear: String
    lateinit var degree: String
    lateinit var major: String
    lateinit var favActivities: String

    override fun onCreate(savedInstanceState: Bundle?) { //Overriding the OnCreate function which is inherited from the AppCompatActivity class
                                                 //Takes an existing Bundle or null as input using variable savedInstanceState.
                                                 // A Bundle is a storage system used to pass data between activities
        super.onCreate(savedInstanceState) //creates a brand new activity using whatever is in the savedInstanceState
        setContentView(R.layout.activity_main) //

        //REFERENCING VIEWS FROM THE activity_main.xml LAYOUT USING THEIR ID's
        spinner = findViewById(R.id.spinnerMajor)
        createButton = findViewById(R.id.create_button)

        spinner.onItemSelectedListener = this //tells the spinner to look in the current class for its listener methods

        radioGroup1 = findViewById(R.id.radioGroup1)
        radioGroup2 = findViewById(R.id.radioGroup2)
        radioGroup3 = findViewById(R.id.radioGroup3)

        //CODE FOR WHEN THE create_button IS PRESSED
        createButton.setOnClickListener {

            firstName = (findViewById<View>(R.id.editTextFirstName) as TextView).text.toString()
            lastName = (findViewById<View>(R.id.editTextLastName) as TextView).text.toString()
            school = (findViewById<View>(R.id.editTextSchool) as TextView).text.toString()
            gradYear = (findViewById<View>(R.id.editTextGradYear) as TextView).text.toString()
            favActivities = (findViewById<View>(R.id.editTextActivities) as TextView).text.toString()

            val intent = Intent(this, BioActivity::class.java).apply {
                putExtra("first_name", firstName)
                putExtra("last_name", lastName)
                putExtra("school", school)
                putExtra("year_of_graduation", gradYear)
                putExtra("selected_degree", degree)
                putExtra("selected_major", major)
                putExtra("favourite_activities", favActivities)
            }
            if (firstName =="" || lastName ==""){
                Toast.makeText(this, R.string.missing_toast, Toast.LENGTH_LONG).show()
            }else{
                startActivity(intent) //Start BioActivity
            }

        }

        // Creating an ArrayAdapter for the spinner using the string array "majors_array" from the strings.xml file as its data source
        val spinnerAdapter = ArrayAdapter.createFromResource(
            this, R.array.majors_array, android.R.layout.simple_spinner_item //Specifying the default spinner layout for the AdapterViews
        ).also { spinnerAdapter ->
            // Specifying the dropdown UI layout to display the adapterViews
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Applying the adapter to the spinner
            spinner.adapter = spinnerAdapter
        }
        //NOTE: Adapters are used to bridge UI components and their data sources
        //Data Source -> Adapter (creates AdapterViews for each element in data source) -> displayed on specified UI View

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long){
        major = parent?.getItemAtPosition(position).toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    fun onRadioButtonPressed(radioButtonView: View){
        val radioGroup = radioButtonView.parent as View

        when (radioGroup.id){
            R.id.radioGroup1 ->{
                radioGroup2.clearCheck()
                radioGroup3.clearCheck()
            }
            R.id.radioGroup2 ->{
                radioGroup1.clearCheck()
                radioGroup3.clearCheck()
            }
            R.id.radioGroup3 ->{
                radioGroup1.clearCheck()
                radioGroup2.clearCheck()
            }
        }

        radioButton = findViewById(radioButtonView.id)
        degree = radioButton.text.toString()
    }




}